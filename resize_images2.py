import os
import glob
import cv2
from os import listdir
from os.path import isfile, join


if __name__ == "__main__":
    raw_dir = 'data/test2/large_dataset/22'
    save_dir = 'data/images/train'
    target_size = (800, 600)
    os.makedirs(save_dir, exist_ok=True)
    for f in listdir(raw_dir):
        if isfile(join(raw_dir, f)) and ('.jpg' in f or '.png' in f or '.jpeg' in f):
            print('f', f)
            img = cv2.imread(join(raw_dir, f))
            img_small = cv2.resize(img, target_size)
            last_index = max([int(f.split('.')[0]) for f in listdir(save_dir)])
            new_fname = "{}.jpg".format(str(last_index + 1))
            small_fname = os.path.join(save_dir, new_fname)
            cv2.imwrite(small_fname, img_small)
