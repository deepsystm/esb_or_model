# ESB_OR_model

ESB object recognition model

### Разметить датасет с картинками
- Сохранить изображения в формате jpg(jpeg) в директорию `./data/raw`.
- Ресайзнуть изображения до единого формата `(800, 600)` с помощью команды
```
python resize_images.py --raw-dir ./data/raw --save-dir ./data/images --ext jpg --target-size "(800, 600)"
```
Расположение изображений с измененным размером в директории `./data/images/`
- Разделить изображения по директориям, `./data/images/train` и `./data/images/test`

- Разметить с помощью приложения [labelImg](https://tzutalin.github.io/labelImg/), которое генерирует `xml` файлы внутри `./data/images/train` и `./data/images/test` директорий.

*Подсказки: можно использовать (`w`: создать область, и т.д.) для ускорения разметки.*

- Закоммитить и запушить (`./data/images/train` и `./data/images/test`) в репозиторий.
