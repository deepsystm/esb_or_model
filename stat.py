from os import listdir
from os.path import isfile, join
import xml.etree.ElementTree as ET


mypath = 'data/images/train'
onlyfiles = [join(mypath, f) for f in listdir(mypath) if '.xml' in f]
names = list()
out = dict()
for f in onlyfiles:
    tree = ET.parse(f)
    root = tree.getroot()
    print(root.find('filename').text)
    names.append(root.find('filename').text)
    out[]
    print([o.find('name').text for o in root.findall('object')])
